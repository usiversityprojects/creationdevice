"""Модуль для создания diy устройства"""
import device_class
import power_class
import cpu_class
import rom_class
import lan_class
import mic_class
import speaker_class


def return_all_modules():
    return "1 - модуль питания\n" \
           "2 - логический модуль\n" \
           "3 - внутренняя память\n" \
           "4 - модуль связи\n" \
           "5 - микрофон\n" \
           "6 - динамики\n"


while True:
    answer = input("1 - узнать о модулях\n"
                   "2 - собрать устройство\n")
    if answer == "1":
        print(f"Укажите модуль, о котором вы хотите узнать:\n"
              f"{return_all_modules()}"
              f"7 - выйти из справочника")
        while True:
            answer_device = input()
            if answer_device == "1":
                power_unit = power_class.Power("вкл")
                power_unit.info()
            if answer_device == "2":
                cpu_unit = cpu_class.Cpu("вкл")
                cpu_unit.info()
            if answer_device == "3":
                rom_unit = rom_class.Rom("вкл")
                rom_unit.info()
            if answer_device == "4":
                lan_unit = lan_class.Lan("вкл")
                lan_unit.info()
            if answer_device == "5":
                mic_unit = mic_class.Microphone("вкл")
                mic_unit.info()
            if answer_device == "6":
                speaker_unit = speaker_class.Speaker("вкл")
                speaker_unit.info()
            elif answer_device == "7":
                break
    if answer == "2":
        menu = input(f"Выберите компоненты для DIY устройства(например 12356):\n"
                     f"Обязательными являются: модуль питания, логический модуль\n"
                     f"{return_all_modules()}")
        if any(i not in "123456" for i in menu):
            print("Вы выбрали модуль, которого нет в списке")
        else:
            device_name = input("Назовите ваше устройство: ")
            diy_device = device_class.Device(device_name, set(menu))
            diy_device.calculate_price()
            diy_device.device_can_do()
