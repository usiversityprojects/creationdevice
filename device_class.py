"""Класс, создающий объект 'устройство'"""
import power_class
import cpu_class
import rom_class
import lan_class
import mic_class
import speaker_class


class Device:
    def __init__(self, name, modules):
        self.name = name
        self.modules = modules
        self.create_price = 30 * len(self.modules)
        self.price = 0
        self.information = []
        self.init_all_devices()
        print(f"Имя устройства: {self.name}")

    def calculate_price(self):
        """
        Метод, который считает стоимость получившегося устройства
        """
        if self.device_is_active():
            self.price += (power_unit.return_values("price") + cpu_unit.return_values("price") +
                           rom_unit.return_values("price") + lan_unit.return_values("price") +
                           mic_unit.return_values("price") + speaker_unit.return_values("price"))
            print(f"Стоимость сборки составила {self.create_price}р, стоимость модулей - {self.price}р, "
                  f"общая стоимость устройства - {self.create_price + self.price}р")

    def device_can_do(self):
        """
        Метод, который выводит все возможности собранного устройства
        """
        if self.device_is_active():
            flag_mic = False
            flag_speaker = False
            self.information.append(power_unit.return_values("func"))
            self.information.append(cpu_unit.return_values("func"))
            self.information.append(rom_unit.return_values("func"))
            self.information.append(lan_unit.return_values("func"))
            if not(rom_unit.return_values("func")) and mic_unit.return_values("func"):
                flag_mic = True
            else:
                self.information.append(mic_unit.return_values("func"))
            if not(rom_unit.return_values("func")) and speaker_unit.return_values("func"):
                flag_speaker = True
            else:
                self.information.append(speaker_unit.return_values("func"))
            for time in range(self.information.count("")):
                self.information.remove("")
            result = ", ".join(self.information)
            print(f"Теперь устройство обладает функциями: {result}")
            if flag_mic:
                print("Микрофон не может работать без внутренней памяти, поэтому устройство "
                      "не имеет его функций, но включает его стоимость")
            if flag_speaker:
                print("Динамики не могут работать без внутренней памяти, поэтому устройство "
                      "не имеет их функций, но включает их стоимость")
        else:
            print("В вашем устройстве нет модуля питания или логического модуля")

    def device_is_active(self):
        """
        Метод, проверяющий существование устройства
        :return: булевое значение
        """
        if "1" in self.modules and "2" in self.modules:
            return True
        return False

    def init_all_devices(self):
        """
        Метод, создающий экземпляры каждого класса модулей
        """
        global power_unit, cpu_unit, rom_unit, lan_unit, mic_unit, speaker_unit
        power_unit = power_class.Power("вкл" if "1" in self.modules else "выкл")
        cpu_unit = cpu_class.Cpu("вкл" if "2" in self.modules else "выкл")
        rom_unit = rom_class.Rom("вкл" if "3" in self.modules else "выкл")
        lan_unit = lan_class.Lan("вкл" if "4" in self.modules else "выкл")
        mic_unit = mic_class.Microphone("вкл" if "5" in self.modules else "выкл")
        speaker_unit = speaker_class.Speaker("вкл" if "6" in self.modules else "выкл")
