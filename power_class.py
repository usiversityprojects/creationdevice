"""Класс, создающий объект 'модуль питания'"""


class Power:

    def __init__(self, toggle):
        self.price = 420
        self.func = "заряжаться и использовать электроэнергию"
        self.toggle = toggle

    def return_values(self, choise):
        """
        Метод для возвращения одно из двух постоянных значений класса
        :param choise: параметр, сообщающий, какое из постоянных
        значений необходимо передать
        :return: постоянное значение класса
        """
        if choise == "price":
            if self.toggle == "вкл":
                return self.price
            else:
                return 0
        elif choise == "func":
            if self.toggle == "вкл":
                return self.func
            else:
                return ""


    def info(self):
        """
        Метод, сообщающий стоимость модуля и его функцию в устрйостве
         """
        print(f"Этот модуль, стоимостью в {self.price}р позволяет заряжать батарею "
              "и использовать заряд для работы других модулей")
