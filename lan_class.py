"""Класс, создающий объект 'модуль связи'"""


class Lan:

    def __init__(self, toggle):
        self.price = 300
        self.func = "выходить в интернет"
        self.toggle = toggle

    def return_values(self, choise):
        """
        Метод для возвращения одно из двух постоянных значений класса
        :param choise: параметр, сообщающий, какое из постоянных
        значений необходимо передать
        :return: постоянное значение класса
        """
        if choise == "price":
            if self.toggle == "вкл":
                return self.price
            else:
                return 0
        elif choise == "func":
            if self.toggle == "вкл":
                return self.func
            else:
                return ""

    def info(self):
        """
        Метод, сообщающий стоимость модуля и его функцию в устрйостве
        """
        print(f"Этот модуль, стоимостью в {self.price}р позволяет выходить в сеть, "
              f"искать информацию и опираться на нее при выполнении действий")
